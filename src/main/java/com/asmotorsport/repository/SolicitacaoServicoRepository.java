package com.asmotorsport.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.asmotorsport.model.SolicitacaoServico;
import com.asmotorsport.model.User;


public interface SolicitacaoServicoRepository extends JpaRepository<SolicitacaoServico, Long> {

	public List<SolicitacaoServico> findByDescricaoContaining(String descricao);

	public List<SolicitacaoServico> findByUser(User user);
	
}
