package com.asmotorsport.repository.filter;

public class SolicitacaoServicoFilter {

	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
