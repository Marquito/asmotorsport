package com.asmotorsport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asmotorsport.model.Password;


@Repository("passwordRepository")
public interface PasswordRepository  extends JpaRepository<Password, Integer> {
	
}

