package com.asmotorsport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asmotorsport.model.Perfil;

@Repository("perfilRepository")
public interface PerfilRepository  extends JpaRepository<Perfil, Long>{

}
