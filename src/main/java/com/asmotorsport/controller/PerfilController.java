package com.asmotorsport.controller;

import java.net.MalformedURLException;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.asmotorsport.model.Arquivo;
import com.asmotorsport.model.User;
import com.asmotorsport.service.FileService;
import com.asmotorsport.service.UserServiceImpl;


@Controller
@RequestMapping("/perfil")
public class PerfilController {
	
	private static final String HOME_VIEW = "perfil";
	private static final String PERFIL_EDITAR = "EditarPerfil";
	private static final String DADOS_CARTAO = "DadosCartao";
	
	@Autowired
	private FileService fileService;
	
	@Autowired private ServletContext context; 
	
	
	@Autowired
	private UserServiceImpl userService;
	
	 private Map<Integer, Arquivo> arquivos;
	

	
		public  PerfilController() {
	
		  }
		
		
		@RequestMapping//(value="/iniciarSessao", method = RequestMethod.GET)
		public ModelAndView home(){
			
			ModelAndView modelAndView = new ModelAndView();
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			modelAndView.addObject("userName", ObterMensagemCabecalho());
			modelAndView.addObject("admin	Message","Data table");
			modelAndView.addObject("bolPossuiFotoPerfil", userService.verificaPossuiImagemPerfil( userService.obterUsuario() ));
			modelAndView.addObject("user",userService.obterUsuario());
			modelAndView.setViewName(HOME_VIEW);
			return modelAndView;
		}
		
		
		@RequestMapping("{user}")
		public ModelAndView edicao(@PathVariable("user") User user) {
			ModelAndView mv = new ModelAndView(PERFIL_EDITAR); 
			mv.addObject("bolPossuiFotoPerfil", userService.verificaPossuiImagemPerfil( userService.obterUsuario() ));
			mv.addObject("userName", ObterMensagemCabecalho());
			mv.addObject("user",user);
			return mv;
		}
		@RequestMapping("/dadosCartao")
		public ModelAndView dadosCartao() {
			ModelAndView modelAndView = new ModelAndView();
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			modelAndView.addObject("userName", ObterMensagemCabecalho());
			modelAndView.addObject("admin	Message","Data table");
			modelAndView.addObject("bolPossuiFotoPerfil", userService.verificaPossuiImagemPerfil( userService.obterUsuario() ));
			modelAndView.addObject("user",userService.obterUsuario());
			modelAndView.setViewName(DADOS_CARTAO);
			return modelAndView;
		}
		
	
		
		private String ObterMensagemCabecalho() {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = userService.findUserByEmail(auth.getName());
			return "Bem vindo a A.S MotorSport " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")";
		}
		 private Object ObterMensagemSucesso() {
			 	return "Perfíl Atualizado com sucesso!";
			
		}
		
		@RequestMapping(method = RequestMethod.POST)
		public ModelAndView salvar(@Validated User user, Errors errors,@PathVariable MultipartFile imgPerfil, RedirectAttributes attributes) throws MalformedURLException, EmailException {
			Boolean bolAlterarArquivo =false;
			/*if (errors.hasErrors()) {
				
					ModelAndView mv = new ModelAndView(PERFIL_EDITAR);
					return mv;
				
			}*/
			try {
				
				MultipartFile inputImage = imgPerfil;
				Long id_user = user.getUser_id(); 
				if(id_user == null) {
					if(inputImage == null) {
							ModelAndView mv = new ModelAndView(PERFIL_EDITAR);
							mv.addObject("fileVazio", "Arquivo é Obrigatório!!!");
							return mv;
					}
				}
				else 
				if(inputImage != null)	
					if(inputImage.getSize() > 0) 
						bolAlterarArquivo=true;
				
				ModelAndView mv = new ModelAndView(HOME_VIEW);
				mv.addObject("user",userService.salvar(user,inputImage,bolAlterarArquivo));
				mv.addObject("userName", ObterMensagemCabecalho());
				mv.addObject("mensagemSucesso", ObterMensagemSucesso());
				return mv;
				
			} catch (IllegalArgumentException e) {
				ModelAndView mv = new ModelAndView(HOME_VIEW);
				return mv;
			}
		}
		
		
}
