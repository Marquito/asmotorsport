package com.asmotorsport.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.asmotorsport.model.Perfil;
import com.asmotorsport.model.Role;
import com.asmotorsport.model.SolicitacaoServico;
import com.asmotorsport.model.User;
import com.asmotorsport.repository.RoleRepository;
import com.asmotorsport.service.UserService;

@Controller
public class LoginController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleRepository roleRepository;
	

	@RequestMapping(value={"/", "/login"}, method = RequestMethod.GET)
	public ModelAndView login(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}
	
	
	@RequestMapping(value="/registration", method = RequestMethod.GET)
	public ModelAndView registration(){
		
		
		Role roleAdmin = roleRepository.findById(1);
		Role roleUSER = roleRepository.findById(2);
		if (roleAdmin == null) {
			Role roleInserir = new Role(1,"ADMIN");
			roleRepository.save(roleInserir);
		
		}
		if (roleUSER == null) {
			Role roleInserirUsuario = new Role(2,"USER");
			roleRepository.save(roleInserirUsuario);
		}
			
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		modelAndView.addObject("user", user);
		modelAndView.setViewName("registration");
		return modelAndView;
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		User userExists = userService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			bindingResult
					.rejectValue("email", "error.user",
							"Informe um email válido!");
		}
		Perfil perfil = new Perfil("","","","","","");
		user.setPerfil(perfil);
		
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("registration");
		} else {
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "Conta criada com sucesso!");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("login");
			
		}
		return modelAndView;
	}
	@RequestMapping(value = "/registrationAdmin", method = RequestMethod.POST)
	public ModelAndView createNewUserAdmin(@Valid User user, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		User userExists = userService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			bindingResult
					.rejectValue("email", "error.user",
							"Informe um email válido!");
		}
		Perfil perfil = new Perfil("","","","","","");
		user.setPerfil(perfil);
		
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("registration");
		} else {
			userService.saveUserAdmin(user);
			modelAndView.addObject("successMessage", "Conta criada com sucesso!");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("registration");
			
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/home", method = RequestMethod.GET)
	public ModelAndView home(){
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		modelAndView.addObject("user", user);
		modelAndView.addObject("userName", "Bem vindo a Chip Tuning " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
		modelAndView.addObject("nome",  user.getName() + " " + user.getLastName());
		modelAndView.addObject("email", " (" + user.getEmail() + ")");
		modelAndView.addObject("adminMessage","Teste");
		modelAndView.setViewName("ordemServico/home");
		return modelAndView;
	}
	

}
