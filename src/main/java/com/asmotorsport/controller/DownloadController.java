package com.asmotorsport.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.asmotorsport.model.Arquivo;
import com.asmotorsport.model.Pasta;
import com.asmotorsport.model.User;
import com.asmotorsport.service.FileService;
import com.asmotorsport.service.UserService;


@Controller
@RequestMapping("/downloads")
public class DownloadController {
	
	private static final String HOME_VIEW = "download";
	
	@Autowired
	private FileService fileService;
	
	@Autowired private ServletContext context; 
	
	
	@Autowired
	private UserService userService;
	
	private Map<Integer, Arquivo> arquivos;
	
	@Value("${asmotorsport.path.arquivosGenuinos}")
	private String arquivosGenuinos;
	

	/*	@ModelAttribute("arquivosParaDownload")
		public List<List<Pasta>> todosStatusTitulo() {
			 File diretorio = new File("C:\\autoFiles\\Arquivo St10");
			return Arrays.asList(fileService.listarArquivos(diretorio));
		}*/
	
		public  DownloadController() {
	
		  }
		
		
		@RequestMapping//(value="/iniciarSessao", method = RequestMethod.GET)
		public ModelAndView home(){
			
			File diretorio = new File(arquivosGenuinos);
			ModelAndView modelAndView = new ModelAndView();
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = userService.findUserByEmail(auth.getName());
			List<Pasta> pastas = fileService.listarPastas(diretorio);
			
			modelAndView.addObject("userName", ObterMensagemCabecalho());
			modelAndView.addObject("admin	Message","Data table");
			modelAndView.addObject("pastas", pastas);
			
			modelAndView.setViewName(HOME_VIEW);
			return modelAndView;
		}
		
		@RequestMapping("/arquivos")
		public ModelAndView novo(@RequestParam("path") String path) {
			File diretorioRaiz = new File(arquivosGenuinos);
			File diretorio = new File(path.replace("\\", "\\\\"));
			ModelAndView modelAndView = new ModelAndView();
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = userService.findUserByEmail(auth.getName());
			List<Pasta> pastas = fileService.listarPastas(diretorioRaiz);
			List<Arquivo> arquivos = fileService.listarArquivos(diretorio);
			
			modelAndView.addObject("userName", ObterMensagemCabecalho());
			modelAndView.addObject("admin	Message","Data table");
			modelAndView.addObject("pastas", pastas);
			modelAndView.addObject("arquivos", arquivos);
			
			modelAndView.setViewName(HOME_VIEW);
			return modelAndView;
		}
		
		
		@RequestMapping(value = "/arquivo", method = RequestMethod.GET)
		public void getFile(HttpServletResponse response,@RequestParam("path") String path,@RequestParam("file") String file )
		{
				
			
			
			
			Path arquivo = Paths.get(path,file);
	        if(Files.exists(arquivo)) {
	        	response.setContentType("text");
	        	response.setHeader("Content-Disposition", "attachment; filename='"+ file +"'");
	        	//response.setHeader("Content-Disposition", "plain/text; charset=utf-8; attachment; filename='"+ solicitacaoServico.getFile().getFile_name()+"'");
	           // response.setHeader("Content-Disposition", "inline");
	           response.setContentType("text");            
	            try {
	                Files.copy(arquivo, response.getOutputStream());
	                response.getOutputStream().flush();
	            } 
	            catch (IOException ex) {
	                ex.printStackTrace();
	            }
	        }


		}
		
		
		private String ObterMensagemCabecalho() {
			
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = userService.findUserByEmail(auth.getName());
			return "Bem vindo a A.S MotorSport " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")";
			
		}
	
}
