package com.asmotorsport.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.asmotorsport.model.Carro;
import com.asmotorsport.model.FachadaTransacaoPayPal;
import com.asmotorsport.model.RespostaSolicitacaoServico;
import com.asmotorsport.model.Role;
import com.asmotorsport.model.SolicitacaoServico;
import com.asmotorsport.model.StatusSolicitacaoServico;
import com.asmotorsport.model.User;
import com.asmotorsport.repository.SolicitacaoServicoRepository;
import com.asmotorsport.service.EmailService;
import com.asmotorsport.service.FileService;
import com.asmotorsport.service.SolicitacaoServicoService;
import com.asmotorsport.service.UserService;
import com.asmotorsport.utils.RoleEnum;




@Controller
@RequestMapping("/solicitacaoServicos")
public class SolicitacaoServicoController {

	private static final String CADASTRO_VIEW = "SolicitacaoServico/CadastroSolicitacaoServico";
	private static final String CADASTRO_DETALHE_VIEW = "SolicitacaoServico/DetalheSolicitacaoServico";
	private static final String CADASTRO_DETALHE_VIEW_ADMIN = "SolicitacaoServico/DetalheSolicitacaoServicoAdmin";
	private static final String CADASTRO_VIEW_RECIBO = "SolicitacaoServico/CadastroSolicitacaoServicoRecibo";
	private static final String HOME_VIEW = "SolicitacaoServico/home";
	private static final String PRECOS = "SolicitacaoServico/Precos";

	@Autowired
	private SolicitacaoServicoService solicitacaoServicoService;

	@Autowired
	private SolicitacaoServicoRepository solicitacaoServicoRepositorio;

	@Autowired
	private UserService userService;

	@Autowired
	private FileService fileService;

	@Autowired
	private ServletContext context;

	@Autowired
	private EmailService emailService;

	@RequestMapping // (value="/home", method = RequestMethod.GET)
	public ModelAndView home() throws Throwable {

		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		List<Role> roles = user.getRoles();
		List<SolicitacaoServico> todasSolicitacoesServicoUsuario = new ArrayList<>();

		//solicitacaoServicoService.atualizarPagamentos();

		if (RoleEnum.USER.getTipoRole().equals(roles.get(0).getId()))
			todasSolicitacoesServicoUsuario = solicitacaoServicoService.obterSolicitacoesDoUsuario(user);
		else
			todasSolicitacoesServicoUsuario = solicitacaoServicoRepositorio.findAll();

		modelAndView.addObject("user", user);
		modelAndView.addObject("perfil", roles.get(0).getRole());
		modelAndView.addObject("userName", userService.obterMensagemCabecalho());
		modelAndView.addObject("admin	Message", "Data table");
		modelAndView.addObject("solicitacaoServicos", todasSolicitacoesServicoUsuario);

		modelAndView.setViewName(HOME_VIEW);
		return modelAndView;
	}

	@RequestMapping("/novo")
	public ModelAndView novo() {
		SolicitacaoServico solicitacaoServico = new SolicitacaoServico();
		Carro carro = new Carro();

		solicitacaoServico.setCarro(carro);

		ModelAndView modelAndView = new ModelAndView(CADASTRO_VIEW);
		modelAndView.addObject("userName", userService.obterMensagemCabecalho());
		modelAndView.addObject(solicitacaoServico);
		return modelAndView;
	}

	@RequestMapping("/precos")
	public ModelAndView precos() throws MalformedURLException, EmailException {
		ModelAndView modelAndView = new ModelAndView(PRECOS);
		return modelAndView;
	}

	@RequestMapping("/enviarEmail")
	public ModelAndView enviarEmail() throws MalformedURLException, EmailException {
		emailService.enviarEmail("teste", "masouzaduarte@gmail.com");
		return null;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView salvar(@Validated SolicitacaoServico solicitacaoServico, Errors errors,
			@PathVariable MultipartFile file_upload, RedirectAttributes attributes)
			throws MalformedURLException, EmailException {
		Boolean bolAlterarArquivo = false;
		if (errors.hasErrors()) {
			ModelAndView mv = new ModelAndView(CADASTRO_VIEW);
			return mv;
		}
		try {
			Long id_Solicitacao_servico = solicitacaoServico.getSolicitacaoServico_id();
			if (id_Solicitacao_servico == null) {
				if (file_upload.isEmpty()) {
					ModelAndView mv = new ModelAndView(CADASTRO_VIEW);
					mv.addObject("fileVazio", "Arquivo é Obrigatório!!!");
					return mv;
				}
			} else if (file_upload.getSize() > 0)
				bolAlterarArquivo = true;

			

			ModelAndView mv = new ModelAndView(CADASTRO_VIEW_RECIBO);
			mv.addObject(solicitacaoServicoService.salvar(userService.obterUsuarioAtual(), solicitacaoServico,
					file_upload, bolAlterarArquivo));
			mv.addObject("userName", userService.obterMensagemCabecalho());
			mv.addObject("mensagemSucesso", ObterMensagemSucesso());
			return mv;

		} catch (IllegalArgumentException e) {
			ModelAndView mv = new ModelAndView(CADASTRO_VIEW);
			return mv;
		}
	}



	@RequestMapping(value = "/salvarArquivoResposta", method = RequestMethod.POST)
	public ModelAndView salvarArquivoResposta(@Validated RespostaSolicitacaoServico solicitacaoServico, Errors errors,
			@PathVariable MultipartFile file_upload, RedirectAttributes attributes)
			throws MalformedURLException, EmailException {
		Boolean bolAlterarArquivo = false;
		if (errors.hasErrors()) {
			ModelAndView mv = new ModelAndView(CADASTRO_VIEW);
			return mv;
		}
		try {
			Long id_Solicitacao_servico = solicitacaoServico.getSolicitacaoServico_id();
			if (id_Solicitacao_servico == null) {
				if (file_upload == null) {
					ModelAndView mv = new ModelAndView(CADASTRO_VIEW);
					mv.addObject("fileVazio", "Arquivo é Obrigatório!!!");
					return mv;
				}
			} else if (file_upload.getSize() > 0)
				bolAlterarArquivo = true;

			ModelAndView mv = new ModelAndView(CADASTRO_VIEW_RECIBO);
			mv.addObject(solicitacaoServicoService.salvarRespostaSolicitacaoServico(userService.obterUsuarioAtual(),
					solicitacaoServico, file_upload, bolAlterarArquivo));
			mv.addObject("userName", userService.obterMensagemCabecalho());
			mv.addObject("mensagemSucesso", ObterMensagemSucesso());
			return mv;

		} catch (IllegalArgumentException e) {
			ModelAndView mv = new ModelAndView(CADASTRO_VIEW);
			return mv;
		}
	}

	private Object ObterMensagemSucesso() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		return "O numero do protocolo foi encaminhado para " + "(" + user.getEmail()
				+ "), confira a baixo os dados da solicitação.";

	}

	private SolicitacaoServico obterUserAutorDaSolicitacaoServico(SolicitacaoServico solicitacaoServico) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		solicitacaoServico.setUser(user);
		return solicitacaoServico;
	}

	/*
	 * @RequestMapping public ModelAndView pesquisar(@ModelAttribute("filtro")
	 * SolicitacaoServicoFilter filtro) { List<SolicitacaoServico>
	 * todasSolicitacoesServico = solicitacaoServicoService.filtrar(filtro);
	 * 
	 * ModelAndView mv = new ModelAndView(HOME_VIEW);
	 * mv.addObject("solicitacaoServicos", todasSolicitacoesServico); return mv; }
	 */
	@RequestMapping("{solicitacaoServico}")
	public ModelAndView edicao(@PathVariable("solicitacaoServico") SolicitacaoServico solicitacaoServico) {
		ModelAndView mv = new ModelAndView(CADASTRO_VIEW);
		mv.addObject("userName", userService.obterMensagemCabecalho());
		mv.addObject(solicitacaoServico);
		return mv;
	}

	@RequestMapping(value = "/excluir/", method = RequestMethod.POST)
	public String excluir(@RequestParam("solicitacaoServico_id") Long solicitacaoServico_id,
			RedirectAttributes attributes) {
		solicitacaoServicoService.excluir(solicitacaoServico_id);

		attributes.addFlashAttribute("mensagem", "Solicitação de serviço excluída com sucesso!");
		return "redirect:/solicitacaoServicos";
	}

	@RequestMapping(value = "/{solicitacaoServico_id}/receber", method = RequestMethod.PUT)
	public @ResponseBody String receber(@PathVariable Long solicitacaoServico_id) {
		return solicitacaoServicoService.receber(solicitacaoServico_id);
	}

	@ModelAttribute("todosStatusSolicitacaoServico")
	public List<StatusSolicitacaoServico> todosStatusTitulo() {
		return Arrays.asList(StatusSolicitacaoServico.values());
	}
	
	
	@Value("${asmotorsport.path.upload}")
	private String upload;

	@RequestMapping(value = "/download/", method = RequestMethod.GET)
	public void getFile(HttpServletResponse response,
			@RequestParam("solicitacaoServico_id") Long solicitacaoServico_id) {

		SolicitacaoServico solicitacaoServico = solicitacaoServicoRepositorio.findOne(solicitacaoServico_id);

		Path arquivo = Paths.get(upload,
				solicitacaoServico.getFile().getFile_id().toString());
		
		
		System.out.printf(upload,
				solicitacaoServico.getFile().getFile_id().toString());
		
		if (!Files.exists(arquivo)) {

			String nomeArquivo = solicitacaoServico.getFile().getFile_name();
			String extensao = nomeArquivo.substring(nomeArquivo.lastIndexOf("."), nomeArquivo.length());

			arquivo = Paths.get(upload,
					solicitacaoServico.getFile().getFile_id().toString() + extensao.toLowerCase());
			
			
			
			
		}
	
	
		if (Files.exists(arquivo)) {
			response.setContentType(solicitacaoServico.getFile().getContent_Type());
			response.setHeader("Content-Disposition",
					"attachment; filename='" + solicitacaoServico.getFile().getFile_name() + "'");
			try {
				Files.copy(arquivo, response.getOutputStream());
				response.getOutputStream().flush();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}

	@Value("${asmotorsport.path.uploadResposta}")
	private String uploadResposta;
	
	
	@RequestMapping(value = "/downloadResposta/", method = RequestMethod.GET)
	public void getFileResposta(HttpServletResponse response,
			@RequestParam("solicitacaoServico_id") Long solicitacaoServico_id) {

		SolicitacaoServico solicitacaoServico = solicitacaoServicoRepositorio.findOne(solicitacaoServico_id);
		Path arquivo = Paths.get(uploadResposta,
				solicitacaoServico.getFile().getFile_id().toString());
	
		if (solicitacaoServico.getStatus() == StatusSolicitacaoServico.ARQUIVO_LIBERADO) {
			if (!Files.exists(arquivo)) {
	
				String nomeArquivo = solicitacaoServico.getFile_resposta().getFile_name();
		
				String extensao = nomeArquivo.substring(nomeArquivo.lastIndexOf("."), nomeArquivo.length());
				arquivo = Paths.get(uploadResposta,
						solicitacaoServico.getFile().getFile_id().toString() + extensao.toLowerCase());
			}
			if (Files.exists(arquivo)) {
				response.setContentType(solicitacaoServico.getFile().getContent_Type());
				response.setHeader("Content-Disposition",
						"attachment; filename='" + solicitacaoServico.getFile_resposta().getFile_name() + "'");
				try {
					Files.copy(arquivo, response.getOutputStream());
					response.getOutputStream().flush();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
				
			}
		}
	}

	@RequestMapping("/detalheSolicitacaoServico/{solicitacaoServico_id}")
	public ModelAndView detalheSolicitacaoServico(@PathVariable("solicitacaoServico_id") Long solicitacaoServico_id) throws Throwable {
		SolicitacaoServico solicitacaoServico = solicitacaoServicoRepositorio.findOne(solicitacaoServico_id);
		ModelAndView mvRetorno = new ModelAndView();
		
		//solicitacaoServicoService.atualizarPagamentos();
		
		if (userService.obterRole().equals(RoleEnum.USER.getTipoRole())) {
			ModelAndView mv = new ModelAndView(CADASTRO_DETALHE_VIEW);
			mv.addObject("userName", userService.obterMensagemCabecalho());
			mv.addObject(solicitacaoServico);
			mvRetorno = mv;
		} else {
			ModelAndView mvAdmin = new ModelAndView(CADASTRO_DETALHE_VIEW_ADMIN);
			mvAdmin.addObject("userName", userService.obterMensagemCabecalho());
			mvAdmin.addObject(solicitacaoServico);
			mvRetorno = mvAdmin;
		}

		return mvRetorno;
	}
	
/*	@RequestMapping(value = "/detalheSolicitacaoServico/receberPagamento", method = RequestMethod.POST)
	public @ResponseBody ModelAndView receberPagamentoPayPal(@ModelAttribute("solicitacaoServico") SolicitacaoServico solicitacaoServico,@ModelAttribute("data")TransacoesPayPal transacaoPayPal) {
		ModelAndView mvRetorno = new ModelAndView();
		
		
		
		if (userService.obterRole().equals(RoleEnum.USER.getTipoRole())) {
			ModelAndView mv = new ModelAndView(CADASTRO_DETALHE_VIEW);
			mv.addObject("userName", userService.obterMensagemCabecalho());
			mv.addObject(solicitacaoServico);
			mvRetorno = mv;
		} else {
			ModelAndView mvAdmin = new ModelAndView(CADASTRO_DETALHE_VIEW_ADMIN);
			mvAdmin.addObject("userName", userService.obterMensagemCabecalho());
			mvAdmin.addObject(solicitacaoServico);
			mvRetorno = mvAdmin;
		}
		//return solicitacaoServicoService.atualizarStatusSolicitacaoServico(solicitacaoServico_id);
		
		
		
		return mvRetorno;
	}*/
    
	@RequestMapping(value="/detalheSolicitacaoServico/receberPagamento",method=RequestMethod.POST)
	@ResponseBody
    public  Boolean  receberPagamentoPayPal(@RequestBody FachadaTransacaoPayPal transacaoPayPal)
    {
		ModelAndView mvRetorno = new ModelAndView();
		return solicitacaoServicoService.atualizarStatusSolicitacaoServico(transacaoPayPal);
    }

}
