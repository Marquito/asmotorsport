package com.asmotorsport.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.asmotorsport.model.SolicitacaoServico;
import com.asmotorsport.repository.SolicitacaoServicoRepository;
import com.asmotorsport.service.PagSeguroService;

import br.com.uol.pagseguro.api.PagSeguro;



@Controller
@RequestMapping("/pagSeguro")
public class PagSeguroController {
		
	
	@Autowired
	PagSeguroService pagSeguroService ;
	
	
	@Autowired
	SolicitacaoServicoRepository solicitacaoServicoRepositorio;
	
	@RequestMapping(value="/obterSessionID", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody br.com.uol.pagseguro.api.session.CreatedSession obterSessionID() {
		try {
			 	
			return pagSeguroService.criarSessaoAplicacao();
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
		
	}
	
	@RequestMapping(value="/processarPagamento", method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Boolean processarPagamento(@Validated String hash,@Validated String token, @Validated Long solicitacaoServico_id,@Validated String nameCard ) {
		try {
			SolicitacaoServico solicitacaoServico =  solicitacaoServicoRepositorio.findOne(solicitacaoServico_id);	
			return pagSeguroService.criarPagamentoDiretoCartaoCredito(hash, token,solicitacaoServico,nameCard);
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
		
	}
	
	
}
