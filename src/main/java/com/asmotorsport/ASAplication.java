package com.asmotorsport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ASAplication {

	public static void main(String[] args) {
		SpringApplication.run(ASAplication.class, args);
	}
}
