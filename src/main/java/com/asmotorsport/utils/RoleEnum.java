package com.asmotorsport.utils;

public enum RoleEnum {

ADMIN(1), USER(2);
	
	Integer  tipoRole ; 
	
	
	public Integer getTipoRole() {
		return tipoRole;
	}


	RoleEnum(Integer tipo){
		tipoRole = tipo; 
	}
}
