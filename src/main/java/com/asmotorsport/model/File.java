package com.asmotorsport.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GeneratorType;

import com.hazelcast.query.impl.getters.GetterFactory;

@Entity
@Table(name = "file")
public class File {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "file_id")
	private Integer file_id;
	
	@Column(name="Content_Type")
	private String Content_Type;
	 
	@OneToOne(mappedBy="file")
	@JoinColumn(name="solicitacaoServico_id")
	private SolicitacaoServico solicitacaoServico;
	
	private	String file_name ;
	
	

	public Integer getFile_id() {
		return file_id;
	}


	public void setFile_id(Integer file_id) {
		this.file_id = file_id;
	}


	public String getFile_name() {
		return file_name;
	}


	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}


	public SolicitacaoServico getSolicitacaoServico() {
		return solicitacaoServico;
	}


	public void setSolicitacaoServico(SolicitacaoServico solicitacaoServico) {
		this.solicitacaoServico = solicitacaoServico;
	}


	public String getContent_Type() {
		return Content_Type;
	}


	public void setContent_Type(String content_Type) {
		Content_Type = content_Type;
	}

	


	
	
	
}
