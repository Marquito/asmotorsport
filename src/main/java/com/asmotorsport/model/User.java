package com.asmotorsport.model;


import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private Long user_id;
	@Column(name = "email")
	@Email(message = "*Informe um e-mail válido !")
	@NotEmpty(message = "*Infome o email!")
	private String email;
	//@Column(name = "password")
	/*@Length(min = 5, message = "*A senha deve possuir pelo menos 5 caracteres!")
	@NotEmpty(message = "*Informe a Senha")*/
/*	@Transient*/
	//private String password;
	
	
	 @OneToOne (cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	 @JoinColumn(name = "perfil_id", referencedColumnName="perfil_id",nullable=true)
	 private Perfil perfil;
	 
	 @Transient
	 @OneToOne(mappedBy="user")
	 @JoinColumn(name="password_id")
	 private Password password;
	 
	
	@Column(name = "name")
	@NotEmpty(message = "*Informe seu Nome!")
	private String name;

	@Column(name = "last_name")
	@NotEmpty(message = "*Informe seu Sobre Nome! ")
	private String lastName;
	
	
	@Column(name = "active")
	private Integer active;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private List<Role> roles;
	

/*
	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_solicitacaoServico", joinColumns = @JoinColumn(name = "user_id"))
	private Set<SolicitacaoServico> solicitacaoServicos;*/
	
	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}



	public Password getPassword() {
		return password;
	}

	public void setPassword(Password password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}
	
	public String getNomeCompleto() {
		return this.name + ' ' + this.lastName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}


	
/*	
	 public Set<SolicitacaoServico> getSolicitacaoServicos() {
		return solicitacaoServicos;
	}

	public void setSolicitacaoServicos(Set<SolicitacaoServico> solicitacaoServicos) {
		this.solicitacaoServicos = solicitacaoServicos;
	}*/



	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	
	

}
