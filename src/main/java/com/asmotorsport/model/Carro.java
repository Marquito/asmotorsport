package com.asmotorsport.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "carro")
public class Carro {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "carro_id")
	private Integer carro_id;
	
	

	@Column(name = "ano")
	@NotEmpty(message = "*Informe o Ano do veículo")
	private String ano;
	
	@Column(name = "Marca")
	@NotEmpty(message = "*Informe a marca do veículo!")
	private String marca;

	@Column(name = "modelo")
	@NotEmpty(message = "*Informe o modelo do veículo!")
	private String modelo;
	
	
	@Column(name = "id_ano")
	@NotEmpty(message = "*Informe o Ano do veículo")
	private String id_ano;
	
	@Column(name = "id_Marca")
	@NotEmpty(message = "*Informe a marca do veículo!")
	private String id_marca;

	@Column(name = "id_modelo")
	@NotEmpty(message = "*Informe o modelo do veículo!")
	private String id_modelo;
	
	@Column(name = "Marca")
	private String versao_modelo;
	

	
	
	@Id
	@GeneratedValue
	@Column(name = "carroId_PK")
	public Integer getCarro_id() {
		return carro_id;
	}

	public void setCarro_id(Integer carro_id) {
		this.carro_id = carro_id;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getVersao_modelo() {
		return versao_modelo;
	}

	public void setVersao_modelo(String versao_modelo) {
		this.versao_modelo = versao_modelo;
	}

	public String getId_ano() {
		return id_ano;
	}

	public void setId_ano(String id_ano) {
		this.id_ano = id_ano;
	}

	public String getId_marca() {
		return id_marca;
	}

	public void setId_marca(String id_marca) {
		this.id_marca = id_marca;
	}

	public String getId_modelo() {
		return id_modelo;
	}

	public void setId_modelo(String id_modelo) {
		this.id_modelo = id_modelo;
	}

	

}
