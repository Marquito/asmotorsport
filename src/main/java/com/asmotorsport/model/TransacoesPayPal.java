package com.asmotorsport.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transacoesPayPal")
public class TransacoesPayPal {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="transacao_id")
	private int id;
	

	@Column(name="orderID")
	private String orderID;
	
	@Column(name="payerID")
	private String payerID;
	
	@Column(name="paymentToken")
	private String paymentToken;
	
	@Column(name="returnUrl")
	private String returnUrl;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getPayerID() {
		return payerID;
	}

	public void setPayerID(String payerID) {
		this.payerID = payerID;
	}

	public String getPaymentToken() {
		return paymentToken;
	}

	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
	
	
	
	
	
}
