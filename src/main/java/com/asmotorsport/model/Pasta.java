package com.asmotorsport.model;

import java.util.List;

public class Pasta {
	
	
	
	private String nome;
	private List<Arquivo> arquivos;
	private String diretorio;
	private List<Pasta> pastas;
	
	
	public Pasta(String nome, List<Arquivo> arquivos, String diretorio, List<Pasta> pastas) {
		super();
		this.nome = nome;
		this.arquivos = arquivos;
		this.diretorio = diretorio;
		this.pastas = pastas;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<Arquivo> getArquivos() {
		return arquivos;
	}
	public void setArquivos(List<Arquivo> arquivos) {
		this.arquivos = arquivos;
	}
	public String getDiretorio() {
		return diretorio;
	}
	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}
	public List<Pasta> getPastas() {
		return pastas;
	}
	public void setPastas(List<Pasta> pastas) {
		this.pastas = pastas;
	}
	
	
	
}
