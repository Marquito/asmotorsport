package com.asmotorsport.model;

public enum StatusSolicitacaoServico {

	PENDENTE("Pendente"),
	AGUARDANDO_PAGAMENTO("Aguardando Pagamento"),
	AGUARDANDO_LIBERACAO("Aguardando Liberação"),
	ARQUIVO_LIBERADO("Arquivo Liberado"),
	RECEBIDO("Recebido");
	
	private String descricao;
	
	StatusSolicitacaoServico(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
