package com.asmotorsport.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "password")
public class Password {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "password_id")
	private Integer password_id;
	
	
	@Column(name = "password")
	@Length(min = 5, message = "*A senha deve possuir pelo menos 5 caracteres!")
	@NotEmpty(message = "*Informe a Senha")
	private String password;
	
	@OneToOne (cascade={CascadeType.REFRESH},fetch=FetchType.EAGER)
	@JoinColumn(name = "user_id", referencedColumnName="user_id",nullable=true)
	private User user;
	
	public Integer getPassword_id() {
		return password_id;
	}

	public void setPassword_id(Integer password_id) {
		this.password_id = password_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Password(User user,String password) {
		this.user = user;
		this.password = password;
	}

	public Password() {

	}
	
	
	
}
