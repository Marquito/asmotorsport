package com.asmotorsport.model;

public class Arquivo {
	
	
	private String nome;
	private Long tamanho;
	private String diretorio;
	
	public Arquivo(String nome, Long tamanho, String diretorio) {
		super();
		this.nome = nome;
		this.tamanho = tamanho;
		this.diretorio = diretorio;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Long getTamanho() {
		return tamanho;
	}
	public void setTamanho(Long tamanho) {
		this.tamanho = tamanho;
	}
	public String getDiretorio() {
		return diretorio;
	}
	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}
	
	
}
