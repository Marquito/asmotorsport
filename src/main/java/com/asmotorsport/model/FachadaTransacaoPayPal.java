package com.asmotorsport.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


public class FachadaTransacaoPayPal {

	
	private Long solicitacaoServico_id;
	private String orderID;
	private String payerID;
	private String paymentToken;
	private String returnUrl;
	
	

	public Long getSolicitacaoServico_id() {
		return solicitacaoServico_id;
	}
	public void setSolicitacaoServico_id(Long solicitacaoServico_id) {
		this.solicitacaoServico_id = solicitacaoServico_id;
	}
	
	public String getOrderID() {
		return orderID;
	}
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	public String getPayerID() {
		return payerID;
	}
	public void setPayerID(String payerID) {
		this.payerID = payerID;
	}
	public String getPaymentToken() {
		return paymentToken;
	}
	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}
	public String getReturnUrl() {
		return returnUrl;
	}
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
	
	
	
	
	
}
