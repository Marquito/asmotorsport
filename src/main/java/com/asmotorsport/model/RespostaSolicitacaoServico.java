package com.asmotorsport.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;



public class RespostaSolicitacaoServico {

	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long solicitacaoServico_id;
	
	
	@Size(max = 60, message = "A descrição não pode conter mais de 60 caracteres")
	private String descricao;
	
	/*@NotNull(message = "Date de processamento é obrigatória")*/
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataProcessamento;
	
	
	/*@NotNull(message = "Valor é obrigatório")*/
	@DecimalMin(value = "0.01", message = "Valor não pode ser menor que 0,01")
	@DecimalMax(value = "9999999.99", message = "Valor não pode ser maior que 9.999.999,99")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal valor;
	
	@Enumerated(EnumType.STRING)
	private StatusSolicitacaoServico status;
	
	 @OneToOne (cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	 @JoinColumn(name = "file_resposta_id", referencedColumnName="file_resposta_id",nullable=true)
	 private FileResposta file_resposta;

	public Long getSolicitacaoServico_id() {
		return solicitacaoServico_id;
	}

	public void setSolicitacaoServico_id(Long solicitacaoServico_id) {
		this.solicitacaoServico_id = solicitacaoServico_id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public StatusSolicitacaoServico getStatus() {
		return status;
	}

	public void setStatus(StatusSolicitacaoServico status) {
		this.status = status;
	}

	public FileResposta getFile_resposta() {
		return file_resposta;
	}

	public void setFile_resposta(FileResposta file_resposta) {
		this.file_resposta = file_resposta;
	}
	
	
}
