package com.asmotorsport.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transacoesPagSeguro")
public class TransacoesPagSeguro {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="transacao_id")
	private int id;
	

	@Column(name="transacoesPagSeguro")
	private String transacoesPagSeguro;
	
	@Column(name="status")
	private String status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTransacoesPagSeguro() {
		return transacoesPagSeguro;
	}
	public void setTransacoesPagSeguro(String transacoesPagSeguro) {
		this.transacoesPagSeguro = transacoesPagSeguro;
	}
	public TransacoesPagSeguro(int id, String transacoesPagSeguro) {
		super();
		this.id = id;
		this.transacoesPagSeguro = transacoesPagSeguro;
	}
	public TransacoesPagSeguro() {
		super();
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
