package com.asmotorsport.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "perfil")
public class Perfil {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "perfil_id")
	private Integer perfil_id;
	
	
	@Column(name = "codigoArea")
	private String codigoArea;
	
	@Column(name = "telefone")
	private String telefone;
	
	@Column(name = "nome_empresa")
	private String nome_empresa;
	
	@Column(name = "cnpj")
	private String cnpj;
	
	@Column(name = "cpf")
	private String cpf;
	

	@Column(name = "endereco_empresa")
	private String endereco_empresa;
	
	@Column(name = "cargo_empresa")
	private String cargo_empresa;
	
	@Column(name = "src_imagem_perfil")
	private String src_imagem_perfil;
	
	
	@OneToOne(mappedBy="perfil")
	@JoinColumn(name="user_id")
	private User user;

	public Perfil() {
		
	}
	
	
	public Perfil( String telefone, String nome_empresa, String cnpj, String endereco_empresa,
			String cargo_empresa, String src_imagem_perfil) {
		super();
		this.telefone = telefone;
		this.nome_empresa = nome_empresa;
		this.cnpj = cnpj;
		this.endereco_empresa = endereco_empresa;
		this.cargo_empresa = cargo_empresa;
		this.src_imagem_perfil = src_imagem_perfil;
	}


	public Integer getPerfil_id() {
		return perfil_id;
	}


	public void setPerfil_id(Integer perfil_id) {
		this.perfil_id = perfil_id;
	}


	public String getTelefone() {
		return telefone;
	}

	public String getTelefoneCompleto() {
		return  this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	public String getNome_empresa() {
		return nome_empresa;
	}


	public void setNome_empresa(String nome_empresa) {
		this.nome_empresa = nome_empresa;
	}


	public String getCnpj() {
		return cnpj;
	}


	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}


	public String getEndereco_empresa() {
		return endereco_empresa;
	}


	public void setEndereco_empresa(String endereco_empresa) {
		this.endereco_empresa = endereco_empresa;
	}


	public String getCargo_empresa() {
		return cargo_empresa;
	}


	public void setCargo_empresa(String cargo_empresa) {
		this.cargo_empresa = cargo_empresa;
	}


	public String getSrc_imagem_perfil() {
		return src_imagem_perfil;
	}


	public void setSrc_imagem_perfil(String src_imagem_perfil) {
		this.src_imagem_perfil = src_imagem_perfil;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}

	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public String getCodigoArea() {
		return codigoArea;
	}


	public void setCodigoArea(String codigoArea) {
		this.codigoArea = codigoArea;
	}


	
	
}
