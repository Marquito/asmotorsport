package com.asmotorsport.model;

public enum MensagemEmail {


		SALVAR("Salvar");
		
		
		private String descricao;
		
		MensagemEmail(String descricao) {
			this.descricao = descricao;
		}
		
		public String getDescricao() {
			return descricao;
		}
		

}
