package com.asmotorsport.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

@Entity
@Table(name = "solicitacaoServico")
public class SolicitacaoServico {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long solicitacaoServico_id;
	
	@NotEmpty(message = "Descrição é obrigatória")
	@Size(max = 60, message = "A descrição não pode conter mais de 60 caracteres")
	private String descricao;
	
	@NotEmpty(message = "O campo Fabricante do módulo é obrigatório!")
	private String modulo_fabricante;

	@NotEmpty(message = "O campo tipo E.G!")
	private String tipo_eg;
	
	
	@NotEmpty(message = "O campo Número do Software é obrigatório!")
	private String numero_software;
	
	
	@NotEmpty(message = "O campo Número do módulo é obrigatório!")
	private String numero_hardware;
	
	@NotEmpty(message = "O campo Número da Atualização é obrigatória!")
	private String numero_update;
	
	@CreationTimestamp	
	/*@NotNull(message = "Date de processamento é obrigatória")*/
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastramento = new java.sql.Date(System.currentTimeMillis());

	/*@NotNull(message = "Date de processamento é obrigatória")*/
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataProcessamento ;
	
	/*@NotNull(message = "Valor é obrigatório")*/
	@DecimalMin(value = "0.01", message = "Valor não pode ser menor que 0,01")
	@DecimalMax(value = "9999999.99", message = "Valor não pode ser maior que 9.999.999,99")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal valor;
	
	@Enumerated(EnumType.STRING)
	private StatusSolicitacaoServico status;
	
	 @ManyToOne
	 @JoinColumn(name="user_id")
	 private User user;
	
	 @OneToOne (cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	 @JoinColumn(name = "file_id", referencedColumnName="file_id",nullable=false)
	 private File file;
	 
	 @OneToOne (cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	 @JoinColumn(name = "file_resposta_id", referencedColumnName="file_resposta_id",nullable=true)
	 private FileResposta file_resposta;
	 	
	@OneToOne(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name="pCarroId_PK")
	 private Carro carro;
	 
	 private String codTransacaoPagamento;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "solicitacaoServico_transacoesPagSeguro", joinColumns = @JoinColumn(name = "solicitacaoServico_id"), inverseJoinColumns = @JoinColumn(name = "transacao_id"))
	private List<TransacoesPagSeguro> transacoesPagSeguro;
	
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "solicitacaoServico_transacoesPayPal", joinColumns = @JoinColumn(name = "solicitacaoServico_id"), inverseJoinColumns = @JoinColumn(name = "transacao_id"))
	private List<TransacoesPayPal> transacoesPayPal;
		
	 
	 
	public List<TransacoesPayPal> getTransacoesPayPal() {
		return transacoesPayPal;
	}

	public void setTransacoesPayPal(List<TransacoesPayPal> transacoesPayPal) {
		this.transacoesPayPal = transacoesPayPal;
	}

	public List<TransacoesPagSeguro> getTransacoesPagSeguro() {
		return transacoesPagSeguro;
	}

	public void setTransacoesPagSeguro(List<TransacoesPagSeguro> transacoesPagSeguro) {
		this.transacoesPagSeguro = transacoesPagSeguro;
	}

	public Carro getCarro() {
		return carro;
	}

	public void setCarro(Carro carro) {
		this.carro = carro;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public Long  getSolicitacaoServico_id() {
		return solicitacaoServico_id;
	}

	public void setSolicitacaoServico_id(Long solicitacaoServico_id	) {
		this.solicitacaoServico_id = solicitacaoServico_id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	

	public Date getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public StatusSolicitacaoServico getStatus() {
		return status;
	}

	public void setStatus(StatusSolicitacaoServico status) {
		this.status = status;
	}
	
	public Boolean isPendente() {
		return StatusSolicitacaoServico.PENDENTE.equals(this.status);
	}
	
	public Boolean isAguardandoPagamento() {
		return StatusSolicitacaoServico.AGUARDANDO_PAGAMENTO.equals(this.status);
	}
	public Boolean isArquivoLiberado() {
		return StatusSolicitacaoServico.ARQUIVO_LIBERADO.equals(this.status);
	}
	

	public String getModulo_fabricante() {
		return modulo_fabricante;
	}

	public void setModulo_fabricante(String modulo_fabricante) {
		this.modulo_fabricante = modulo_fabricante;
	}
	
	

	public String getTipo_eg() {
		return tipo_eg;
	}

	public void setTipo_eg(String tipo_eg) {
		this.tipo_eg = tipo_eg;
	}

	public String getNumero_software() {
		return numero_software;
	}

	public void setNumero_software(String numero_software) {
		this.numero_software = numero_software;
	}

	public String getNumero_hardware() {
		return numero_hardware;
	}

	public void setNumero_hardware(String numero_hardware) {
		this.numero_hardware = numero_hardware;
	}

	public String getNumero_update() {
		return numero_update;
	}

	public void setNumero_update(String numero_update) {
		this.numero_update = numero_update;
	}
		
	
	public Date getDataCadastramento() {
		return dataCadastramento;
	}

	public void setDataCadastramento(Date dataCadastramento) {
		this.dataCadastramento = new Date();
	
	}
	
	
	public String getCodTransacaoPagamento() {
		return codTransacaoPagamento;
	}

	public void setCodTransacaoPagamento(String codTransacaoPagamento) {
		this.codTransacaoPagamento = codTransacaoPagamento;
	}

	public FileResposta getFile_resposta() {
		return file_resposta;
	}

	public void setFile_resposta(FileResposta file_resposta) {
		this.file_resposta = file_resposta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((solicitacaoServico_id == null) ? 0 : solicitacaoServico_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SolicitacaoServico other = (SolicitacaoServico) obj;
		if (solicitacaoServico_id == null) {
			if (other.solicitacaoServico_id != null)
				return false;
		} else if (!solicitacaoServico_id.equals(other.solicitacaoServico_id))
			return false;
		return true;
	}

}
