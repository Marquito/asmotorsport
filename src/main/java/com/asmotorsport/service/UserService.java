package com.asmotorsport.service;

import com.asmotorsport.model.User;

public interface UserService {
	public User findUserByEmail(String email);
	public void saveUser(User user);
	public User obterUsuarioAtual();
	void saveUserAdmin(User user);
	public String obterMensagemCabecalho();
	public Integer obterRole();
	public User obterUsuario(); 
}
