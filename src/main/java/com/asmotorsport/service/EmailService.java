package com.asmotorsport.service;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.ImageHtmlEmail;
import org.apache.commons.mail.resolver.DataSourceUrlResolver;
import org.springframework.stereotype.Service;

import com.asmotorsport.model.SolicitacaoServico;



@Service("emailService")
public class EmailService {

	
public void enviarEmail(String txtMensagem, String txtUsuarioDestinatario) throws EmailException, MalformedURLException {
	
	
	// carregue seu modelo de e-mail HTML
	 // String htmlEmailTemplate = ".... <img src = \" http://localhost:8090/images/email.jpg\"> ....";
	String htmlEmailTemplate = "<h2><img alt=\"\" height=\"83\" src=\" http://localhost/images/email.jpg\" width=\"300\" /></h2>\r\n" + 
			"\r\n" + 
			"<h2>Este email foi gerado automaticamente!</h2>\r\n" + 
			"\r\n" + 
			"<p>" + txtMensagem  +"</p>\r\n" + 
			"\r\n" + 
			"<address>&nbsp;</address>\r\n" + 
			"";
	  // defina seu URL base para resolver locais de recursos relativos
	  URL url = new URL ("http://localhost");
	
	
	ImageHtmlEmail email = new ImageHtmlEmail ();
	email.setDataSourceResolver (new DataSourceUrlResolver (url));
	email.setDebug(true);
	email.setHostName("smtp.googlemail.com");
	email.setSmtpPort (587);
	email.setAuthenticator(new DefaultAuthenticator("masouzaduarte@asmotorsportss.com","c8h2p6m5"));
	//para funcionar é preciso que o anti virus esteja desabilitado 
	//email.setSSLOnConnect(true);
	email.setStartTLSRequired(true);
	email.addTo(txtUsuarioDestinatario); //pode ser qualquer um email
	email.setFrom("masouzaduarte@asmotorsportss.com"); //aqui necessita ser o email que voce fara a autenticacao
	email.setSubject("A.S MOTOR SPORT");
	// define a mensagem html
	  email.setHtmlMsg (htmlEmailTemplate);

	  // define a mensagem alternativa
	  email.setTextMsg ("Seu cliente de email não suporta mensagens HTML");

	  // manda o email
	  email.send ();
	  //para funcionar é preciso que o anti virus esteja desabilitado 
	
}


public String obterMensagemEmailSalvar(SolicitacaoServico solicitacaoServico) {
	// TODO Auto-generated method stub
	return "Sua solicitação de serviço foi cadastrada com sucesso em "+ solicitacaoServico.getDataCadastramento() +" Você podera acompanhar sua solicitação de serviço pelo Numero: " + solicitacaoServico.getSolicitacaoServico_id();
}


public String obterMensagemEmailSalvarResposta(SolicitacaoServico solicitacaoServico) {
	// TODO Auto-generated method stub
	return "Sua solicitação de serviço foi processada com sucesso em "+ solicitacaoServico.getDataProcessamento() +" Você podera acompanhar sua solicitação de serviço pelo Numero: " + solicitacaoServico.getSolicitacaoServico_id();
}
	
	
	
}
