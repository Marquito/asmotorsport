package com.asmotorsport.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.ServletContext;

//import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.asmotorsport.model.Arquivo;
import com.asmotorsport.model.Pasta;



//import org.springframework.web.multipart.commons.CommonsMultipartFile;

@Service("fileService")
public class FileService {
	
	  

	  @Autowired private ServletContext context; 
	  
	  private Map<Integer, Arquivo> arquivos;
	  
	public String  uploadArquivo(MultipartFile file,String fileId,String path_diretorio) {
	       try {
	    	   String extensaoDoArquivo = FilenameUtils.getExtension(file.getOriginalFilename());
	    	   File diretorio = new File(path_diretorio); // ajfilho é uma pasta!
	    	   if (!diretorio.exists()) {
	    	      diretorio.mkdirs(); 
	    	   } 
	    	   
	            // Get the file and save it somewhere
	            byte[] bytes = file.getBytes();
	            Path path = Paths.get( path_diretorio + fileId + "." + extensaoDoArquivo.toLowerCase());
	            Files.write(path, bytes);
	            
 	            return path_diretorio + fileId + "." + extensaoDoArquivo;
	       } catch (IOException e) {	    	   
	            e.printStackTrace();
	        }
		return "Arquivo com Erro!";     
	}
	
	public boolean deletarArquivo(Integer file_id,String path_diretorio) {
		
		Path arquivo = Paths.get(path_diretorio,file_id.toString());
		
		if(Files.exists(arquivo)) {
			File file = new File (arquivo.toString());
			return file.delete();
		}
		return false;
			
	}
	public boolean deletarImagemPerfil(String file_id,String path_diretorio) {
		
		Path arquivo = Paths.get(path_diretorio,file_id);
		
		if(Files.exists(arquivo)) {
			File file = new File (arquivo.toString());
			return file.delete();
		}
		return false;
			
	}
	
	
	public  List<Arquivo> listarArquivos(File source) 
	{	
		
		List<Arquivo> arquivos = new ArrayList<>();
	    List<File> fileList = new ArrayList<>();

	    File[] list = source.listFiles();
	    for (File fl : list) {
	        if (!fl.isDirectory()) {
	        	
	            fileList.add(fl);
	            Arquivo arquivo = new Arquivo(fl.getName(), fl.getTotalSpace(),fl.getParentFile().getAbsolutePath());	
	            arquivos.add(arquivo);
	          
	       
	        }
	    }
	    return arquivos;
	}
	
	public  List<Pasta> listarPastas (File source) 
	{	
		
		List<Pasta> pastas = new ArrayList<>();
	    File[] list = source.listFiles();
	    
	    for (File fl : list) {
	        if (fl.isDirectory()) {
	        	Pasta pasta = new Pasta(" " + fl.getName(),null,fl.getAbsolutePath(), null); 
	        	pastas.add(pasta);
	        }
	    }
	    
	    return pastas;
	}
	

}
