package com.asmotorsport.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.ServletContext;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.asmotorsport.model.Password;
import com.asmotorsport.model.Role;
import com.asmotorsport.model.User;
import com.asmotorsport.repository.PasswordRepository;
import com.asmotorsport.repository.PerfilRepository;
import com.asmotorsport.repository.RoleRepository;
import com.asmotorsport.repository.UserRepository;
@Service("userService")
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PerfilRepository perfilRepository;
	@Autowired
	private PasswordRepository passwordRepository;
	
	@Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired 
    private ServletContext context; 
    @Autowired 
    private FileService fileService; 
    
	  
    
	@Override
	public User findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}


	@Override
	public void saveUser(User user) {
	    user.setActive(1);
        Role userRole = roleRepository.findByRole("USER");
         List<Role> roles = new ArrayList<>(); 
         roles.add(userRole);
        user.setRoles(roles);
		Password password = new Password(userRepository.save(user),bCryptPasswordEncoder.encode(user.getPassword().getPassword()));
		passwordRepository.save(password);
	}
	
	@Override
	public void saveUserAdmin(User user) {
	    user.setActive(1);
        Role userRole = roleRepository.findByRole("ADMIN");
        List<Role>roles  = new ArrayList<>();
        roles.add(userRole);
        user.setRoles(roles);
		Password password = new Password(userRepository.save(user),bCryptPasswordEncoder.encode(user.getPassword().getPassword()));
		passwordRepository.save(password);
	}
	
	public User obterUsuarioAtual() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = findUserByEmail(auth.getName());
		return user;
	}

	@Value("${asmotorsport.path.imagensPerfil}")
	private String imagensPerfil;
	
	@SuppressWarnings("null")
	public User salvar(User user, MultipartFile file_upload, Boolean bolAlterarArquivo) {
		String extensaoDoArquivo = FilenameUtils.getExtension(file_upload.getOriginalFilename());
		String pathImagemPerfil = context.getRealPath("/resources") + "/imagesPerfil/";
		
		
		if(bolAlterarArquivo) 
			fileService.deletarImagemPerfil(user.getUser_id() + "." + FilenameUtils.getExtension(user.getPerfil().getSrc_imagem_perfil()),context.getRealPath("/resources") + "/imagesPerfil/");
		
		User userAtualizado = new User();
	    
		user.setActive(1);
	    //Role userRole = roleRepository.findByRole("ADMIN");
	    Role userRoleUser = roleRepository.findByRole("USER");
	    List<Role> roles= new ArrayList<>();
	    //roles.add(userRole);
	    roles.add(userRoleUser);
        user.setRoles(roles);
        
        if(bolAlterarArquivo) 
			user.getPerfil().setSrc_imagem_perfil("/resources/imagesPerfil/" + user.getUser_id().toString() + "." + extensaoDoArquivo);

		userAtualizado = userRepository.save(user);
		
		 if(bolAlterarArquivo) 
			fileService.uploadArquivo(file_upload,userAtualizado.getUser_id().toString(),pathImagemPerfil);

		return userAtualizado;
	}

	public Boolean verificaPossuiImagemPerfil(User user) {
		return !user.getPerfil().getSrc_imagem_perfil().equals("");
	}
	
	public String obterMensagemCabecalho() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = this.findUserByEmail(auth.getName());
		return "Bem vindo a A.S MotorSport " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")";

	}
	
	public Integer obterRole() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = this.findUserByEmail(auth.getName());
		return user.getRoles().get(0).getId();

	}
	public User obterUsuario() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return this.findUserByEmail(auth.getName());
		
	}
	
	
}
