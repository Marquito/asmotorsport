package com.asmotorsport.service;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.asmotorsport.model.FachadaTransacaoPayPal;
import com.asmotorsport.model.File;
import com.asmotorsport.model.FileResposta;
import com.asmotorsport.model.RespostaSolicitacaoServico;
import com.asmotorsport.model.SolicitacaoServico;
import com.asmotorsport.model.StatusSolicitacaoServico;
import com.asmotorsport.model.TransacoesPagSeguro;
import com.asmotorsport.model.TransacoesPayPal;
import com.asmotorsport.model.User;
import com.asmotorsport.repository.SolicitacaoServicoRepository;
import com.autoFiles.repository.filter.SolicitacaoServicoFilter;



@Service
public class SolicitacaoServicoService {

	@Autowired
	private SolicitacaoServicoRepository solicitacaoServicoRepositorio;
	
	@Autowired 
	private EmailService emailService; 
	
	@Autowired
	private FileService fileService;
	
	@Autowired 
	private ServletContext context; 
	
	@Autowired
	PagSeguroService pagSeguroService;
	
	@Value("${asmotorsport.path.upload}")
	private String upload;
	
	
	public SolicitacaoServico salvar(User user, SolicitacaoServico solicitacaoServicoDoCliente,MultipartFile file,Boolean bolAlterarArquivo) throws MalformedURLException, EmailException {
		try {
			SolicitacaoServico solicitacaoServicoAlteracao = null;
			String upload_local = context.getRealPath("/resources") + "/upload/";
			if ( solicitacaoServicoDoCliente.getSolicitacaoServico_id()!=null) {
				solicitacaoServicoAlteracao= solicitacaoServicoRepositorio.findOne(solicitacaoServicoDoCliente.getSolicitacaoServico_id());
			
				if(bolAlterarArquivo) {
					alterarArquivoDaSolicitacaoServico(solicitacaoServicoAlteracao.getFile().getFile_id(),upload);
				}
				
			}	
			
			SolicitacaoServico solicitacaoServicoAtualizada = new SolicitacaoServico();
			File fileUpload = new File();
			fileUpload.setFile_name(file.getOriginalFilename());
			fileUpload.setContent_Type(file.getContentType());
			solicitacaoServicoDoCliente.setUser(user);
			solicitacaoServicoDoCliente.setStatus(StatusSolicitacaoServico.PENDENTE);
			
			if(file.getSize() > 0)	
				solicitacaoServicoDoCliente.setFile(fileUpload);
			else 
				solicitacaoServicoDoCliente.setFile(solicitacaoServicoAlteracao.getFile());
				
			solicitacaoServicoAtualizada = solicitacaoServicoRepositorio.save(solicitacaoServicoDoCliente);
			fileService.uploadArquivo(file, solicitacaoServicoAtualizada.getFile().getFile_id().toString(),upload);
			
			emailService.enviarEmail(emailService.obterMensagemEmailSalvar(solicitacaoServicoAtualizada), user.getEmail());
			
			return solicitacaoServicoAtualizada;
		} catch (DataIntegrityViolationException e) {
			throw new IllegalArgumentException("Formato de data inválido");
		}
	}
	
	
	@Value("${asmotorsport.path.uploadResposta}")
	private String uploadResposta;
	public SolicitacaoServico salvarRespostaSolicitacaoServico(User user, RespostaSolicitacaoServico respostaSolicitacaoServicoDoCliente ,MultipartFile file,Boolean bolAlterarArquivo) throws MalformedURLException, EmailException {
		try {
			SolicitacaoServico solicitacaoServicoAlteracao = null;
			SolicitacaoServico solicitacaoServicoDoCliente = solicitacaoServicoRepositorio.getOne(respostaSolicitacaoServicoDoCliente.getSolicitacaoServico_id());
			String uploadRespostaLocal = context.getRealPath("/resources") + "/upload/";
			 Date dataCadastramento = new java.sql.Date(System.currentTimeMillis());
			
			solicitacaoServicoDoCliente.setStatus(respostaSolicitacaoServicoDoCliente.getStatus());
			solicitacaoServicoDoCliente.setDescricao(respostaSolicitacaoServicoDoCliente.getDescricao());
			solicitacaoServicoDoCliente.setDataProcessamento(respostaSolicitacaoServicoDoCliente.getDataProcessamento());
			solicitacaoServicoDoCliente.setValor(respostaSolicitacaoServicoDoCliente.getValor());
			solicitacaoServicoDoCliente.setDataProcessamento(dataCadastramento);
			
			if ( solicitacaoServicoDoCliente.getSolicitacaoServico_id()!=null) {
				solicitacaoServicoAlteracao= solicitacaoServicoRepositorio.findOne(solicitacaoServicoDoCliente.getSolicitacaoServico_id());
			
				if(bolAlterarArquivo) {
					alterarArquivoDaSolicitacaoServico(solicitacaoServicoAlteracao.getFile().getFile_id(),uploadResposta);
				}
				
			}	
			
			SolicitacaoServico solicitacaoServicoAtualizada = new SolicitacaoServico();
			FileResposta fileUpload = new FileResposta();
			fileUpload.setFile_name(file.getOriginalFilename());
			fileUpload.setContent_Type(file.getContentType());
			solicitacaoServicoDoCliente.setUser(solicitacaoServicoAlteracao.getUser());
			solicitacaoServicoDoCliente.setFile(solicitacaoServicoAlteracao.getFile());
			
			if(file.getSize() > 0)	
				solicitacaoServicoDoCliente.setFile_resposta(fileUpload);
			else 
				solicitacaoServicoDoCliente.setFile_resposta(solicitacaoServicoAlteracao.getFile_resposta());
				
			solicitacaoServicoAtualizada = solicitacaoServicoRepositorio.save(solicitacaoServicoDoCliente);
			fileService.uploadArquivo(file, solicitacaoServicoAtualizada.getFile().getFile_id().toString(),uploadResposta);
			
			emailService.enviarEmail(emailService.obterMensagemEmailSalvarResposta(solicitacaoServicoAtualizada), user.getEmail());
			
			return solicitacaoServicoAtualizada;
		} catch (DataIntegrityViolationException e) {
			throw new IllegalArgumentException("Formato de data inválido");
		}
	}

	private Boolean alterarArquivoDaSolicitacaoServico(Integer file_id, String path_diretorio) {
		// TODO Auto-generated method stub
		return fileService.deletarArquivo(file_id,path_diretorio);
	}


	public void excluir(Long codigo) {
		solicitacaoServicoRepositorio.delete(codigo);
	}

	public String receber(Long solicitacaoServico_id) {
		SolicitacaoServico solicitacaoServico = solicitacaoServicoRepositorio.findOne(solicitacaoServico_id);
		solicitacaoServico.setStatus(StatusSolicitacaoServico.ARQUIVO_LIBERADO);
		solicitacaoServicoRepositorio.save(solicitacaoServico);
		
		return StatusSolicitacaoServico.ARQUIVO_LIBERADO.getDescricao();
	}
	
	public List<SolicitacaoServico> filtrar(SolicitacaoServicoFilter filtro) {
		String descricao = filtro.getDescricao() == null ? "%" : filtro.getDescricao();
		return solicitacaoServicoRepositorio.findByDescricaoContaining(descricao);
	}
	public List<SolicitacaoServico> obterSolicitacoesDoUsuario(User user) {

		return solicitacaoServicoRepositorio.findByUser(user);
	}
	public void atualizarPagamentos() throws Throwable {
		List<SolicitacaoServico> solicitacoes = solicitacaoServicoRepositorio.findAll();
		for (SolicitacaoServico solicitacaoServico : solicitacoes) {
			if (solicitacaoServico.getStatus().equals(StatusSolicitacaoServico.AGUARDANDO_LIBERACAO)) {
				List<TransacoesPagSeguro> transacoes = solicitacaoServico.getTransacoesPagSeguro();
				List<TransacoesPagSeguro> transacoesAtualizadas = new ArrayList<>();
				for (TransacoesPagSeguro transacoesPagSeguro : transacoes) {
					transacoesPagSeguro  = pagSeguroService.atualizarStatusTransacao(transacoesPagSeguro);
					if(transacoesPagSeguro.getStatus().equals("APPROVED") || transacoesPagSeguro.getStatus().equals("AVAILABLE")) {
						solicitacaoServico.setStatus(StatusSolicitacaoServico.ARQUIVO_LIBERADO);
					}
					
					transacoesAtualizadas.add(transacoesPagSeguro);
				}
				
				solicitacaoServico.setTransacoesPagSeguro(transacoesAtualizadas);
				solicitacaoServicoRepositorio.save(solicitacaoServico);
			}
		}
		
	}
	public Boolean atualizarStatusSolicitacaoServico(FachadaTransacaoPayPal transacaoPayPalRecebida) {
		
		SolicitacaoServico solicitacaoServico = solicitacaoServicoRepositorio.findOne(transacaoPayPalRecebida.getSolicitacaoServico_id());
		TransacoesPayPal transacaoPayPal = new TransacoesPayPal();
		
		
		List<TransacoesPayPal> transacoes = new ArrayList<>();
		try {
			transacaoPayPal.setOrderID(transacaoPayPalRecebida.getOrderID());
			transacaoPayPal.setPayerID(transacaoPayPalRecebida.getPayerID());
			transacaoPayPal.setPaymentToken(transacaoPayPalRecebida.getPaymentToken());
			transacaoPayPal.setReturnUrl(transacaoPayPalRecebida.getReturnUrl());
			
			transacoes.add(transacaoPayPal);
			
			solicitacaoServico.setTransacoesPayPal(transacoes);
			
			if(transacaoPayPalRecebida.getPaymentToken() != "") {
				solicitacaoServico.setStatus(StatusSolicitacaoServico.ARQUIVO_LIBERADO);
			}else {
				return false;
			}
			 solicitacaoServicoRepositorio.save(solicitacaoServico);
			 
			 return true;
		} catch (Exception e) {
			return false ;
			// TODO: handle exception
		}
		
	
		
	}
}
