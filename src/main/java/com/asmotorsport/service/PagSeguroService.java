package com.asmotorsport.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asmotorsport.model.Role;
import com.asmotorsport.model.SolicitacaoServico;
import com.asmotorsport.model.StatusSolicitacaoServico;
import com.asmotorsport.model.TransacoesPagSeguro;
import com.asmotorsport.model.User;
import com.asmotorsport.repository.SolicitacaoServicoRepository;
import com.asmotorsport.repository.UserRepository;

import br.com.uol.pagseguro.api.PagSeguro;
import br.com.uol.pagseguro.api.PagSeguroEnv;
import br.com.uol.pagseguro.api.common.domain.BankName;
import br.com.uol.pagseguro.api.common.domain.DataList;
import br.com.uol.pagseguro.api.common.domain.ShippingType;
import br.com.uol.pagseguro.api.common.domain.builder.AddressBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.BankBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.CreditCardBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.DateRangeBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.DocumentBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.HolderBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.InstallmentBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.PaymentItemBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.PhoneBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.PreApprovalCreditCardBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.PreApprovalHolderBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.PreApprovalPaymentMethodBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.PreApprovalRequestBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.SenderBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.ShippingBuilder;
import br.com.uol.pagseguro.api.common.domain.enums.Currency;
import br.com.uol.pagseguro.api.common.domain.enums.DocumentType;
import br.com.uol.pagseguro.api.common.domain.enums.PreApprovalPaymentMethodType;
import br.com.uol.pagseguro.api.common.domain.enums.State;
import br.com.uol.pagseguro.api.credential.Credential;
import br.com.uol.pagseguro.api.direct.preapproval.AccededDirectPreApproval;
import br.com.uol.pagseguro.api.direct.preapproval.DirectPreApprovalAccessionBuilder;
import br.com.uol.pagseguro.api.direct.preapproval.DirectPreApprovalCancellationBuilder;
import br.com.uol.pagseguro.api.exception.PagSeguroBadRequestException;
import br.com.uol.pagseguro.api.exception.ServerError;
import br.com.uol.pagseguro.api.exception.ServerErrors;
import br.com.uol.pagseguro.api.http.JSEHttpClient;
import br.com.uol.pagseguro.api.preapproval.PreApprovalRegistrationBuilder;
import br.com.uol.pagseguro.api.preapproval.RegisteredPreApproval;
import br.com.uol.pagseguro.api.preapproval.cancel.CancelledPreApproval;
import br.com.uol.pagseguro.api.preapproval.cancel.PreApprovalCancellationBuilder;
import br.com.uol.pagseguro.api.session.CreatedSession;
import br.com.uol.pagseguro.api.transaction.register.DirectPaymentRegistrationBuilder;
import br.com.uol.pagseguro.api.transaction.search.TransactionDetail;
import br.com.uol.pagseguro.api.utils.logging.SimpleLoggerFactory;
/*
https://github.com/pagseguro/pagseguro-java-sdk/tree/master/public/example-api/src/main/java/br/com/uol/pagseguro/example/api


https://github.com/pagseguro/pagseguro-java-sdk/blob/master/integration_test/src/test/java/integration/Modulos/Transactions.java
*/

@Service("pagSeguroService")
public class PagSeguroService {

	public String sellerEmail = "masouzaduarte@gmail.com";
	public String sellerToken = "E9FC1B3EA4A64BF3A2EC83E157D451BA";

	@Autowired
	UserRepository usuarioRepositorio;
	
	@Autowired
	SolicitacaoServicoRepository solicitacaoServicoRepositorio;
	
	public Boolean criarPagamentoDiretoCartaoCredito(String hash, String token, SolicitacaoServico solicitacaoServico, String nameCard) throws ParseException {

		try {

			final PagSeguro pagSeguro = PagSeguro.instance(new SimpleLoggerFactory(), new JSEHttpClient(),
					Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);
			User usuario = solicitacaoServico.getUser();
			
			
			TransactionDetail transactionCard = pagSeguro.transactions()
					.register(new DirectPaymentRegistrationBuilder().withPaymentMode("default")
							.withCurrency(Currency.BRL)
							.addItem(new PaymentItemBuilder().withId(solicitacaoServico.getSolicitacaoServico_id().toString()).withDescription("Solicitação de Serviço Nº" + solicitacaoServico.getSolicitacaoServico_id())
									.withQuantity(1).withAmount(solicitacaoServico.getValor()))
							.withNotificationURL("www.lojateste.com.br/notification").withReference("Solicitacao Numero " + solicitacaoServico.getSolicitacaoServico_id())
							.withSender(new SenderBuilder().withName(usuario.getName() + " " + usuario.getLastName()).withCPF(usuario.getPerfil().getCpf())
									.withPhone(new PhoneBuilder().withAreaCode(usuario.getPerfil().getCodigoArea()).withNumber(usuario.getPerfil().getTelefone())

									).withEmail("c46451620941629136839@sandbox.pagseguro.com.br").withHash(hash)// alterar
																												// conforme
																												// a
																												// session
							)
							.withShipping(new ShippingBuilder()
									.withAddress(new AddressBuilder().withStreet("Nao se Aplica").withNumber("1")
											.withComplement("Nao se Aplica").withDistrict("Nao se Aplica")
											.withPostalCode("14800360").withCity("Nao se Aplica").withState(State.DF)
											.withCountry("BRA"))
									.withType(ShippingType.Type.SEDEX).withCost(new BigDecimal(0))))
					.withCreditCard(new CreditCardBuilder()
							.withBillingAddress(new AddressBuilder().withStreet("Nao se Aplica").withNumber("Nao se Aplica")
									.withComplement("teste").withDistrict("Centro").withPostalCode("99999999")
									.withCity("Nao se Aplica").withState(State.SP).withCountry("BRA"))
							.withInstallment(new InstallmentBuilder().withQuantity(1).withValue(solicitacaoServico.getValor())
									.withNoInterestInstallmentQuantity(2))
							.withHolder(new HolderBuilder()
									.addDocument(
											new DocumentBuilder().withType(DocumentType.CPF).withValue(usuario.getPerfil().getCpf()))
									.withName(nameCard)
									.withBithDate(new SimpleDateFormat("dd/MM/yyyy").parse("25/03/1995"))
									.withPhone(new PhoneBuilder().withAreaCode(usuario.getPerfil().getCodigoArea()).withNumber(usuario.getPerfil().getTelefone())))
							.withToken(token));

			String codTransacaoPagamento = transactionCard.getCode();
			 List<TransacoesPagSeguro> transacoes= new ArrayList<>();
			 TransacoesPagSeguro transacao = new TransacoesPagSeguro();
			 transacao.setTransacoesPagSeguro(codTransacaoPagamento);
			 transacoes.add(transacao);
			solicitacaoServico.setStatus(StatusSolicitacaoServico.AGUARDANDO_LIBERACAO); 
			solicitacaoServico.setCodTransacaoPagamento(codTransacaoPagamento);
			solicitacaoServico.setTransacoesPagSeguro(transacoes);
			solicitacaoServicoRepositorio.save(solicitacaoServico);
			System.out.println(codTransacaoPagamento);

		} catch (PagSeguroBadRequestException e) {

		}
		return null;

	}

	public void criarPagamentoDiretoCartaoDebito() {

		final PagSeguro pagSeguro = PagSeguro.instance(new SimpleLoggerFactory(), new JSEHttpClient(),
				Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

		try {
			// Checkout transparente (pagamento direto) com debito online
			TransactionDetail onlineDebitTransaction = pagSeguro.transactions()
					.register(new DirectPaymentRegistrationBuilder().withPaymentMode("default")
							.withCurrency(Currency.BRL).withExtraAmount(new BigDecimal(100.00))
							.addItem(new PaymentItemBuilder().withId("0001").withDescription("Produto PagSeguroI")
									.withAmount(new BigDecimal(99999.99)).withQuantity(1).withWeight(1000))

							.addItem(new PaymentItemBuilder().withId("0002").withDescription("Produto PagSeguroII")
									.withAmount(new BigDecimal(99999.98)).withQuantity(2).withWeight(750))
							.withNotificationURL("www.sualoja.com.br/notification")
							.withReference("LIBJAVA_DIRECT_PAYMENT")
							.withSender(new SenderBuilder().withEmail("c46451620941629136839@sandbox.pagseguro.com.br")
									.withName("Jose Comprador").withCPF("99999999999")
									/*
									 * Para saber como obter o valor do Hash, acesse:
									 * https://devs.pagseguro.uol.com.br/docs/checkout-web-usando-a-sua-tela#obter-
									 * identificacao-do-comprador
									 */
									.withHash("2NpHD19EV81c08RW")
									.withPhone(new PhoneBuilder().withAreaCode("99").withNumber("99999999")))
							.withShipping(
									new ShippingBuilder().withType(ShippingType.Type.SEDEX).withCost(BigDecimal.TEN)
											.withAddress(new AddressBuilder().withPostalCode("99999999")
													.withCountry("BRA").withState(State.SP).withCity("Cidade Exemplo")
													.withComplement("99o andar").withDistrict("Jardim Internet")
													.withNumber("9999").withStreet("Av. PagSeguro"))))
					.withOnlineDebit(new BankBuilder().withName(BankName.Name.BANCO_DO_BRASIL));
			System.out.println(onlineDebitTransaction);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void criarPagamentoDiretoBoletoBancario() {

		final PagSeguro pagSeguro = PagSeguro.instance(new SimpleLoggerFactory(), new JSEHttpClient(),
				Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

		try {
			// Checkout transparente (pagamento direto) com boleto
			TransactionDetail bankSlipTransaction = pagSeguro.transactions()
					.register(new DirectPaymentRegistrationBuilder().withPaymentMode("default")
							.withCurrency(Currency.BRL).withExtraAmount(new BigDecimal(100.00))
							.addItem(new PaymentItemBuilder().withId("0001").withDescription("Produto PagSeguroI")
									.withAmount(new BigDecimal(99999.99)).withQuantity(1).withWeight(1000))

							.addItem(new PaymentItemBuilder().withId("0002").withDescription("Produto PagSeguroII")
									.withAmount(new BigDecimal(99999.98)).withQuantity(2).withWeight(750))
							.withNotificationURL("www.sualoja.com.br/notification")
							.withReference("LIBJAVA_DIRECT_PAYMENT")
							.withSender(new SenderBuilder().withEmail("comprador@uol.com.br").withName("Jose Comprador")
									.withCPF("99999999999")
									/*
									 * Para saber como obter o valor do Hash, acesse:
									 * https://devs.pagseguro.uol.com.br/docs/checkout-web-usando-a-sua-tela#obter-
									 * identificacao-do-comprador
									 */
									.withHash("abc123")
									.withPhone(new PhoneBuilder().withAreaCode("99").withNumber("99999999")))
							.withShipping(
									new ShippingBuilder().withType(ShippingType.Type.SEDEX).withCost(BigDecimal.TEN)
											.withAddress(new AddressBuilder().withPostalCode("99999999")
													.withCountry("BRA").withState(State.SP).withCity("Cidade Exemplo")
													.withComplement("99o andar").withDistrict("Jardim Internet")
													.withNumber("9999").withStreet("Av. PagSeguro"))))
					.withBankSlip();
			System.out.println(bankSlipTransaction);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void criarAssinatura() {

		try {

			final PagSeguro pagSeguro = PagSeguro.instance(new SimpleLoggerFactory(), new JSEHttpClient(),
					Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

			// Assinatura
			RegisteredPreApproval registeredPreApproval = pagSeguro.preApprovals()
					.register(new PreApprovalRegistrationBuilder().withCurrency(Currency.BRL)
							.withExtraAmount(BigDecimal.ONE).withReference("XXXXXX").withSender(new SenderBuilder()//
									.withEmail("comprador@uol.com.br")//
									.withName("Jose Comprador").withCPF("99999999999").withPhone(new PhoneBuilder()//
											.withAreaCode("99") //
											.withNumber("99999999"))) //
							.withShipping(new ShippingBuilder()//
									.withType(ShippingType.Type.SEDEX) //
									.withCost(BigDecimal.TEN)//
									.withAddress(new AddressBuilder() //
											.withPostalCode("99999999").withCountry("BRA").withState(State.SP)//
											.withCity("Cidade Exemplo").withComplement("99o andar")
											.withDistrict("Jardim Internet").withNumber("9999")
											.withStreet("Av. PagSeguro")))
							.withPreApproval(new PreApprovalRequestBuilder().withCharge("auto")
									.withName("Seguro contra roubo do Notebook Rosa")
									.withDetails("Cada dia 28 será cobrado o valor de R$100,00 referente ao seguro "
											+ "contra roubo do Notebook Prata")
									.withAmountPerPayment(BigDecimal.TEN).withMaxTotalAmount(new BigDecimal(200))
									.withMaxAmountPerPeriod(BigDecimal.TEN).withMaxPaymentsPerPeriod(2)
									.withPeriod("monthly")
									.withDateRange(new DateRangeBuilder().between(new Date(),
											DatatypeConverter.parseDateTime("2018-09-27T23:59:59.000-03:00")
													.getTime())))
							.withRedirectURL("http://loja.teste.com/redirect")
							.withNotificationURL("http://loja.teste.com/notification")

			);
			System.out.println(registeredPreApproval.getRedirectURL());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void cancelarAssinatura() {

		try {

			final PagSeguro pagSeguro = PagSeguro.instance(new SimpleLoggerFactory(), new JSEHttpClient(),
					Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

			// Cancelamento de assinaturas
			CancelledPreApproval cancelledPreApproval = pagSeguro.preApprovals()
					.cancel(new PreApprovalCancellationBuilder().withCode("F50E50A8B5B5743AA4E67F8D78D11A62"));
			System.out.println(cancelledPreApproval.getTransactionStatus());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void buscarAssinatura() {
		String sellerEmail = "your_seller_email";
		String sellerToken = "your_seller_token";

		final PagSeguro pagSeguro = PagSeguro.instance(new SimpleLoggerFactory(), new JSEHttpClient(),
				Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

		try {

			// Busca de assinaturas
			DataList dataList = pagSeguro.preApprovals().search()
					.byDateRange(new DateRangeBuilder().between(
							DatatypeConverter.parseDateTime("2016-10-01T00:00:00.000-03:00").getTime(),
							DatatypeConverter.parseDateTime("2016-10-03T15:56:00.000-03:00").getTime()), 1, 10);
			System.out.println(dataList);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void aderirPlano() {

		try {

			final PagSeguro pagSeguro = PagSeguro.instance(new SimpleLoggerFactory(), new JSEHttpClient(),
					Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

			// Aderindo ao plano FFAC8AE62424AC5884C90F8DAAE2F21A
			AccededDirectPreApproval accededDirectPreApproval = pagSeguro.directPreApprovals()
					.accede(new DirectPreApprovalAccessionBuilder().withPlan("FFAC8AE62424AC5884C90F8DAAE2F21A")
							.withReference("XXXXXXXX")
							.withSender(new SenderBuilder().withName("José Comprador")
									.withEmail("senderemail@sandbox.pagseguro.com.br").withIp("1.1.1.1")
									.withHash("HASHHERE")
									.withPhone(new PhoneBuilder().withAreaCode("99").withNumber("99999999"))
									.withAddress(new AddressBuilder().withStreet("Av. PagSeguro").withNumber("9999")
											.withComplement("99o andar").withDistrict("Jardim Internet")
											.withCity("Cidade Exemplo").withState(State.SP).withCountry("BRA")
											.withPostalCode("99999999"))
									.addDocument(
											new DocumentBuilder().withType(DocumentType.CPF).withValue("99999999999")))

							.withPaymentMethod(new PreApprovalPaymentMethodBuilder()
									.withType(PreApprovalPaymentMethodType.CREDITCARD)
									.withCreditCard(new PreApprovalCreditCardBuilder()
											.withToken("bdb962e9b432483f8e12d62c3fc4adc5")
											.withHolder(new PreApprovalHolderBuilder().withName("JOSÉ COMPRADOR")
													.withBirthDate(
															new SimpleDateFormat("dd/MM/yyyy").parse("20/12/1990"))
													.addDocument(new DocumentBuilder().withType(DocumentType.CPF)
															.withValue("99999999999"))
													.withPhone(new PhoneBuilder().withAreaCode("99")
															.withNumber("99999999"))
													.withBillingAddress(new AddressBuilder().withStreet("Av. PagSeguro")
															.withNumber("9999").withComplement("99o andar")
															.withDistrict("Jardim Internet").withCity("Cidade Exemplo")
															.withState(State.SP).withCountry("BRA")
															.withPostalCode("99999999"))))));

			System.out.println(accededDirectPreApproval.getPreApprovalCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void cancelarAdesao() {

		try {
			final PagSeguro pagSeguro = PagSeguro.instance(new SimpleLoggerFactory(), new JSEHttpClient(),
					Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

			// Cancelamento de adesão (assinatura)
			pagSeguro.directPreApprovals()
					.cancel(new DirectPreApprovalCancellationBuilder().withCode("1EA91B6DFFFFC60FF4030F807D2473EE"));

			System.out.println("Cancelamento de adesão realizado!");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public CreatedSession criarSessaoAplicacao() {

		try {
			String sellerEmail = "masouzaduarte@gmail.com";
			String sellerToken = "E9FC1B3EA4A64BF3A2EC83E157D451BA";

			final PagSeguro pagSeguro = PagSeguro.instance(new SimpleLoggerFactory(), new JSEHttpClient(),
					Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

			// Criacao de sessao de Aplicacao
			CreatedSession createdSessionApplication = pagSeguro.sessions().create("E9FC1B3EA4A64BF3A2EC83E157D451BA");
			return createdSessionApplication;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public void reembolsar() {
		try {
			String sellerEmail = "your_seller_key";
			String sellerToken = "your_seller_token";

			final PagSeguro pagSeguro = PagSeguro.instance(Credential.sellerCredential(sellerEmail, sellerToken),
					PagSeguroEnv.SANDBOX);

			/* devolve o valor de R$ 10.90 para uma transacao */
			pagSeguro.transactions().refundByCode("TRANSACATIONCODEHERE", new BigDecimal(10.90));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		// Cenario: Consultar transacoes por codigo invailda
		// Dado que esteja autenticado na api do pagseguro
		// Quando crio uma requisicao de pagamento
		// E pesquiso a transacao pelo codigo invalido
		// Entao e retornado um erro de consulta por codigo

		public TransactionDetail pesquiso_transacao_codigo(String codTransacao) throws Throwable {

			try {
				final PagSeguro pagSeguro = PagSeguro.instance(
						Credential.sellerCredential(sellerEmail, sellerToken),
						PagSeguroEnv.SANDBOX);

				
				TransactionDetail transactionDetail = pagSeguro.transactions()
						.search().byCode(codTransacao);
				
				return transactionDetail;

			} catch (PagSeguroBadRequestException e) {
				System.out.println(e.getErrors());

				ServerErrors serverErros = e.getErrors();
				ServerError serverError = serverErros.getErrors().iterator().next();
				return null;
				/*assertEquals("invalid transactionCode value: 212212121",
						serverError.getMessage());
				assertEquals(new Integer(13003), serverError.getCode());*/

			}

		}

		public TransacoesPagSeguro atualizarStatusTransacao(TransacoesPagSeguro transacoesPagSeguro) throws Throwable {
			// TODO Auto-generated method stub
			TransactionDetail transacao =  pesquiso_transacao_codigo(transacoesPagSeguro.getTransacoesPagSeguro());
			transacoesPagSeguro.setStatus(transacao.getStatus().getStatus().toString());;
			return transacoesPagSeguro;
		}


	

}
