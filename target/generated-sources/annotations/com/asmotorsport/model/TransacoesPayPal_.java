package com.asmotorsport.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TransacoesPayPal.class)
public abstract class TransacoesPayPal_ {

	public static volatile SingularAttribute<TransacoesPayPal, String> paymentToken;
	public static volatile SingularAttribute<TransacoesPayPal, String> orderID;
	public static volatile SingularAttribute<TransacoesPayPal, String> payerID;
	public static volatile SingularAttribute<TransacoesPayPal, Integer> id;
	public static volatile SingularAttribute<TransacoesPayPal, String> returnUrl;

}

