package com.asmotorsport.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Perfil.class)
public abstract class Perfil_ {

	public static volatile SingularAttribute<Perfil, String> telefone;
	public static volatile SingularAttribute<Perfil, String> codigoArea;
	public static volatile SingularAttribute<Perfil, String> endereco_empresa;
	public static volatile SingularAttribute<Perfil, String> cargo_empresa;
	public static volatile SingularAttribute<Perfil, Integer> perfil_id;
	public static volatile SingularAttribute<Perfil, String> cpf;
	public static volatile SingularAttribute<Perfil, String> src_imagem_perfil;
	public static volatile SingularAttribute<Perfil, String> nome_empresa;
	public static volatile SingularAttribute<Perfil, String> cnpj;
	public static volatile SingularAttribute<Perfil, User> user;

}

