package com.asmotorsport.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(File.class)
public abstract class File_ {

	public static volatile SingularAttribute<File, String> Content_Type;
	public static volatile SingularAttribute<File, String> file_name;
	public static volatile SingularAttribute<File, Integer> file_id;
	public static volatile SingularAttribute<File, SolicitacaoServico> solicitacaoServico;

}

