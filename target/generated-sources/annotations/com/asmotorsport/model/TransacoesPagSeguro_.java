package com.asmotorsport.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TransacoesPagSeguro.class)
public abstract class TransacoesPagSeguro_ {

	public static volatile SingularAttribute<TransacoesPagSeguro, String> transacoesPagSeguro;
	public static volatile SingularAttribute<TransacoesPagSeguro, Integer> id;
	public static volatile SingularAttribute<TransacoesPagSeguro, String> status;

}

