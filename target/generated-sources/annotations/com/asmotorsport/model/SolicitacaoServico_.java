package com.asmotorsport.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SolicitacaoServico.class)
public abstract class SolicitacaoServico_ {

	public static volatile SingularAttribute<SolicitacaoServico, Long> solicitacaoServico_id;
	public static volatile ListAttribute<SolicitacaoServico, TransacoesPayPal> transacoesPayPal;
	public static volatile SingularAttribute<SolicitacaoServico, String> modulo_fabricante;
	public static volatile SingularAttribute<SolicitacaoServico, FileResposta> file_resposta;
	public static volatile SingularAttribute<SolicitacaoServico, Date> dataCadastramento;
	public static volatile SingularAttribute<SolicitacaoServico, BigDecimal> valor;
	public static volatile SingularAttribute<SolicitacaoServico, Date> dataProcessamento;
	public static volatile SingularAttribute<SolicitacaoServico, String> descricao;
	public static volatile SingularAttribute<SolicitacaoServico, String> numero_update;
	public static volatile SingularAttribute<SolicitacaoServico, String> tipo_eg;
	public static volatile SingularAttribute<SolicitacaoServico, String> codTransacaoPagamento;
	public static volatile SingularAttribute<SolicitacaoServico, Carro> carro;
	public static volatile ListAttribute<SolicitacaoServico, TransacoesPagSeguro> transacoesPagSeguro;
	public static volatile SingularAttribute<SolicitacaoServico, File> file;
	public static volatile SingularAttribute<SolicitacaoServico, String> numero_software;
	public static volatile SingularAttribute<SolicitacaoServico, String> numero_hardware;
	public static volatile SingularAttribute<SolicitacaoServico, User> user;
	public static volatile SingularAttribute<SolicitacaoServico, StatusSolicitacaoServico> status;

}

