package com.asmotorsport.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Carro.class)
public abstract class Carro_ {

	public static volatile SingularAttribute<Carro, String> marca;
	public static volatile SingularAttribute<Carro, String> ano;
	public static volatile SingularAttribute<Carro, String> id_ano;
	public static volatile SingularAttribute<Carro, String> id_modelo;
	public static volatile SingularAttribute<Carro, Integer> carro_id;
	public static volatile SingularAttribute<Carro, String> versao_modelo;
	public static volatile SingularAttribute<Carro, String> id_marca;
	public static volatile SingularAttribute<Carro, String> modelo;

}

