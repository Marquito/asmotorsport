package com.asmotorsport.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Password.class)
public abstract class Password_ {

	public static volatile SingularAttribute<Password, String> password;
	public static volatile SingularAttribute<Password, Integer> password_id;
	public static volatile SingularAttribute<Password, User> user;

}

