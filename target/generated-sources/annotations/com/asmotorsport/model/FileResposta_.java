package com.asmotorsport.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FileResposta.class)
public abstract class FileResposta_ {

	public static volatile SingularAttribute<FileResposta, String> Content_Type;
	public static volatile SingularAttribute<FileResposta, String> file_name;
	public static volatile SingularAttribute<FileResposta, Integer> file_resposta_id;
	public static volatile SingularAttribute<FileResposta, SolicitacaoServico> solicitacaoServico;

}

