

	$("#validarCartao")
			.click(
					function() {

						console.log("oi");

						$
								.ajax({
									type : 'GET',
									headers : {
										'X-No-Cache' : '1'
									},
									url : 'http://localhost:8090/pagSeguro/obterSessionID',
									dataType : "json",
									statusCode : {
										200 : function(pagSeguroSessionId) {
											console
													.log('Session ID criado pelo PagSeguro: '
															+ pagSeguroSessionId.id);

											PagSeguroDirectPayment
													.setSessionId(pagSeguroSessionId.id);

											//Chama o código no backend que vai submeter de fato para a URI de transactions do PagSeguro.
											//  processarPagamentoPagSeguro(AppStore.session.pedido, senderHash,function(clonePedido,erro){

											var numcard = $("#cartao").val();
											var bin = numcard.substr(0, 6);
											console.log(numcard);
											var brand;
											PagSeguroDirectPayment
													.getBrand({
														cardBin : numcard,
														success : function(
																response) {
															//bandeira encontrada
															console
																	.log(response);
															console
																	.log("################");
															console
																	.log(response.brand.name);
															brand = response.brand.name;

															PagSeguroDirectPayment
																	.onSenderHashReady(function(
																			response) {
																		if (response.status == 'error') {
																			console
																					.log(response.message);
																			return false;
																		}

																		var hash = response.senderHash; //Hash estará disponível nesta variável.

																		console
																				.log(hash);

																		var cartao = {
																			numero : $(
																					"#cartao")
																					.val(),
																			brand : brand,
																			cvv : $(
																					"#cvv")
																					.val(),
																			validadeMonth : $(
																					"#validadeMes")
																					.val(),
																			validadeYear : $(
																					"#validadeAno")
																					.val()
																		};

																		console
																				.log(cartao);
																		PagSeguroDirectPayment
																				.createCardToken({
																					cardNumber : $(
																							"#cartao")
																							.val(),
																					brand : brand,
																					cvv : $(
																							"#cvv")
																							.val(),
																					expirationMonth : $(
																							"#validadeMes")
																							.val(),
																					expirationYear : $(
																							"#validadeAno")
																							.val(),
																					success : function(
																							response) {
																						//token gerado, esse deve ser usado na chamada da API do Checkout Transparente
																						console
																								.log(response);
																						console
																								.log("Vou Efetuar Pagamento");
																						var token = response.card.token;

																						processarPagamento(
																								hash,
																								token);

																					},
																					error : function(
																							response) {
																						//tratamento do erro
																							
																						console
																								.log("2");
																					},
																					complete : function(
																							response) {
																						//tratamento comum para todas chamadas
																						console
																								.log("3");
																					}
																				});

																	});

														},
														error : function(
																response) {
															//tratamento do erro
															console
																	.log("erro com o cartão");
															console
																	.log(response);
														},
														complete : function(
																response) {
															//tratamento comum para todas chamadas
															console
																	.log(response);
														}
													});

										},
										400 : function(r) {
											callback('Falha ao se comunicar com o PagSeguro, por favor, tente novamente.');
										}
									}
								});

					});

	function processarPagamento(hash, token) {
		console.log("############processarPagamento###########");
		console.log(hash);
		console.log(token);
		console.log($("#solicitacaoServico_id").val());
		solicitacaoServico_id = $("#solicitacaoServico_id").val();
		nameCard = $("#nameCard").val();
		console.log(nameCard);

		$.ajax({
			url : "http://localhost:8090/pagSeguro/processarPagamento",
			type : 'post',
			data : {
				hash : hash,
				token : token,
				solicitacaoServico_id : solicitacaoServico_id,
				nameCard : nameCard
			},
			success : function(data) {
				console.log("RETORNO PROCESSAR PAGAMENTO");
				console.log(data);
				mensagemSucesso();
			},
			error : function(response) {
				//tratamento do erro
				console.log(response);
				console.log("erro ao processar pagamento");
			},
			complete : function(response) {
				//tratamento comum para todas chamadas
				console.log("COMPLETO");
				console.log("3");
			}
		});

	}
	function mensagemSucesso(){
		
		console.log("oi recebi a solicitação de pagamento");
		swal({
            title: "Recebemos seu pagamento!",
            text: "Aguarde a liberação pelo PagSeguro!",
            type: "success"
        });
	};
	function mensagemErro(){
		
		console.log("oi recebi a solicitação de pagamento");
		swal({
            title: "Erro ao processar pagamento!",
            text: "Houve erro no processo de pagamento!",
            type: "warning"
        });
	};
	
	$("#botaoComprar").click(function() {
		console.log("COMPRAR");

		PagSeguroDirectPayment.onSenderHashReady(function(response) {
			if (response.status == 'error') {
				console.log(response.message);
				return false;
			}

			var hash = response.senderHash; //Hash estará disponível nesta variável.
			console.log(hash);

			PagSeguroDirectPayment.createCardToken({
				cardNumber : $("#cartao").val(),
				brand : "visa",
				cvv : $("#cvv").val(),
				expirationMonth : $("#validadeMes").val(),
				expirationYear : $("#validadeAno").val(),
				success : function(response) {
					//token gerado, esse deve ser usado na chamada da API do Checkout Transparente
					console.log(response);
					console.log("1");
				},
				error : function(response) {
					//tratamento do erro
					console.log(response);
					console.log("2");
				},
				complete : function(response) {
					//tratamento comum para todas chamadas
					console.log("3");
				}
			});

		});
	});
